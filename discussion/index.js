// CRUD OPERATIONS

/*
- CRUD operations are the heart of any backend application
- CRUD stands for Create, Read, Update and Delete
- MongoDB deals with objects as it's structure for documents, we can easily create them by providing objects into our methods
*/


// CREATE: Inserting documents

// INSERT ONE
/*
Syntax:
	db.collectionName/insertOne({})

Inserting/Assigning value in JS Objects:
	object.object.method({object})
	*/

	db.users.insertOne({
		firstName: "Jane",
		lastName: "Doe",
		age: 21,
		contact: {
			phone: "5875698",
			email: "janedoe@gmail.com"
		},
		course: ["CSS", "Javascript", "Python"],
		deparment: "none"
	})

// INSERT MANY
/*
Syntax:
	db.users.insertMany([
		{objectA},
		{objectB}
	])
	*/

	db.users.insertMany([
	{
		firstName: "Stephen",
		lastName: "Hawking",
		age: 76,
		contact: {
			phone: "5878787",
			email: "stephenhawking@gmail.com"
		},
		courses: ["Pythin", "React", "PHP"],
		deparment: "none"
	},
	{
		firstName: "Neil",
		lastName: "Armstrong",
		age: 82,
		contact: {
			phone: "5552252",
			email: "stephenhawking@gmail.com"
		},
		courses: ["React", "Laravel", "SASS"],
		deparment: "none"
	}
	])





// READ: FINDING DOCUMENTS

// FIND ALL
/*
Syntax:
	db.collectionName.find();
	*/
	db.users.find();

// Finding users with single arguments
/*
Syntax:
	db.collectionName.find({ field: value })
	*/

// Finding users with multiple argumments
// With no matches
db.users.find({firstName: "Stephen"})

// With matching one
db.users.find({firstName: "Stephen", age: 20}); // Fetched 0 record(s) in 40ms
db.users.find({firstName: "Stephen", age: 76});

// Update: Updating documents
// Repeat Jane to be updated
db.users.insertOne({
	firstName: "Jane",
	lastName: "Doe",
	age: 21,
	contact: {
		phone: "5875698",
		email: "janedoe@gmail.com"
	},
	course: ["CSS", "Javascript", "Python"],
	deparment: "none"
});

// UPDATE ONE
/*
Syntax:
	db.collectionName.updateOne({criteria} , {$set: (field: value)})
	*/
	db.users.updateOne(
		{firstName: "Jane"},
		{
			$set: {
				firstName: "Jane",
				lastName: "Gates",
				age: 65,
				contact: {
					phone: "0000000",
					email: "janegates@gmail.com"
				}, 
				courses: ["AWS", "Google Cloud", "Azure"],
				department: "infrastructure",
				status: "active"
			}
		}

		);
	db.users.find({firstName: "Jane"});

//UPDATE MANY
db.users.updateMany(
	{department: "none"},
	{
		$set: {
			deparment: "HR"
		}
	}
	)
db.users.find().pretty();

// REPLACE ONE
/*
- Can be used if replacing the whole document is necessary
- Syntax:
	db.collectionName.replaceOne( {criteria}, {$set: {field: value}} )
	*/
db.users.replaceOne(
	{ lastName: "Gates" },
        {
			firstName: "Bill",
			lastName: "Clinton"
	}
)
db.users.find({firstName: "Bill"});


// DELETE: DELETING DOCUMENTS
db.users.insert({
	firstName: "Test",
	lastName: "Test",
	age: 0,
	contact: {
		phone: "000000",
		email: "test@gmail.com"
	},
	course: [],
	department: "none"
})
db.users.find({firstName: "Test"});

// Deleting a single document
/*
Syntax:
	db.collectionName.deleteOne({criteria})
*/
db.users.deleteOne({firstName: "Test"});
db.users.find({firstName: "Test"});

db.users.deleteOne({age: "76"});
db.users.find({age: "76"});

// Delete many
db.users.deleteMany({
	course: []
});
db.users.find();

// Delete all
// db.users.delete()
/*
- Be careful when using the "deleteMany" method. If no search criteria is provided, it will deletee all documents
- DO NOT USE: db.collectionName.deleteMany()
- Syntax:
	db.collectionName.deleteManay({criteria})
*/

// ADVANCED QUERIES

// Query an embeded document
db.users.find({
	contact: {
		phone: "5875698",
		email: "janedoe@gmail.com"
	},
})

// Find the document with the email "janedoe@gmail.com"
// Querying one nested fields
db.users.find( {"contact.email": "janedoe@gmail.com"} )

// Queryin an Array with exact elements
db.users.find({courses: ["React", "Laravel", "SASS"]});

// Quering a document without regards to order of the array
db.users.find({courses: {$all: ["React", "Laravel", "SASS"]}});

// Make an array to querry
db.users.insert({
	namearr: [
		{
			namea: "juan"
		},
		{
			nameb: "tamad"
		}
	]
});

// find
db.users.find({
	namearr: {
		namea: "juan"
	}
});